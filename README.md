# *hMod* heuristic framework

*hMod* is a Java-based library for modeling and implementing algorithms, including heuristic methods. hMod provides a detailed object-oriented representation of algorithms through a pattern-like implementation. Data, operation and structure of algorithms can be implemented as decoupled entities along with their combination in a flexible approach. 

For more information, please visit the current repository at https://github.com/eurra/hmod.